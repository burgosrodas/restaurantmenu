package david.burgos.menu.fragments;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import david.burgos.menu.POJO.Category;
import david.burgos.menu.POJO.SubCategory;
import david.burgos.menu.R;


public class MainFragment extends Fragment {

    private static final String CATEGORY_ID = "id";
    private static final String CATEGORY_NAME = "name";
    private static final String CATEGORY_IMAGE = "img_path";
    private static final String CATEGORY_TYPE_ID = "type_id";
    private static final String CATEGORY_ENABLED = "enabled";
    private static final String CATEGORY_CREATED = "created_at";
    private static final String CATEGORY_UPDATED = "updated_at";
    private static final String CATEGORY_SUBCATEGORY = "subcategory";

    private static final String SUBCATEGORY_ID = "id";
    private static final String SUBCATEGORY_NAME = "name";
    private static final String SUBCATEGORY_CATEGORY_ID = "category_id";
    private static final String SUBCATEGORY_ADDITION_ENABLE = "addition_enable";
    private static final String SUBCATEGORY_ENABLED = "enabled";
    private static final String SUBCATEGORY_CREATED_AT = "created_at";
    private static final String SUBCATEGORY_UPDATED_AT = "updated_at";

    AppCompatEditText mTextViewRepos;
    private static URL DetailsURL;
    private static URL CategoryURL;

    public static List<Category> CategoryList;

    private static final String LOG_TAG = MainFragment.class.getSimpleName();

    public MainFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        BindingViews(rootView);
        new FetchInfoTask().execute();
        return rootView;
    }

    private void BindingViews(View rootView) {
        mTextViewRepos = (AppCompatEditText)rootView.findViewById(R.id.editText);
    }

    private String readFullResponse(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String response ="";
        String line;
        while((line = bufferedReader.readLine()) != null){
            stringBuilder.append(line).append("\n");
        }
        if(stringBuilder.length() > 0){
            response = stringBuilder.toString();
        }
        return response;
    }

    private void FillURLs(String response) {
        final String REQUEST = "requests";

        List<String> repos = new ArrayList<>();
        try {
            JSONObject responseJsonObject = new JSONObject(response);
            JSONArray jsonArray;
            JSONObject jsonObject;
            jsonArray = responseJsonObject.getJSONArray(REQUEST);

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.get("name").equals("Category")) {
                    DetailsURL = new URL(jsonObject.getString("url"));
                }
                if (jsonObject.get("name").equals("Categories")) {
                    CategoryURL = new URL(jsonObject.getString("url"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    class FetchInfoTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            mTextViewRepos.setText(response);
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = ConstructURLQuery();
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                try {
                    String response = readFullResponse(httpsURLConnection.getInputStream());
                    FillURLs(response);
                } catch(IOException e){
                    e.printStackTrace();
                }finally{
                    httpsURLConnection.disconnect();
                }

                if(CategoryURL != null){
                    HttpURLConnection httpCategoryURLConnection = (HttpURLConnection) CategoryURL.openConnection();
                    try {
                        String CategoryResponse = readFullResponse(httpCategoryURLConnection.getInputStream());
                        CreateCategoriesFromJSON(CategoryResponse);
                    }catch(IOException e){
                        e.printStackTrace();
                    }finally{
                        httpCategoryURLConnection.disconnect();
                    }
                }

                if(DetailsURL != null){
                    HttpURLConnection httpDetailsURLConnection = (HttpURLConnection) DetailsURL.openConnection();

                    try {
                        String DetailsResponse = readFullResponse(httpDetailsURLConnection.getInputStream());
                    }catch(IOException e){
                        e.printStackTrace();
                    }finally{
                        httpDetailsURLConnection.disconnect();
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }
    }

    private void CreateCategoriesFromJSON(String categoryResponse) {
        if(categoryResponse != null){
            try {
                JSONArray jsonArray = new JSONArray(categoryResponse);
                CategoryList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    
                    JSONObject CategoryObject = jsonArray.getJSONObject(i);
                    
                    Category category = new Category();
                    category.setId(CategoryObject.getString(CATEGORY_ID));
                    category.setName(CategoryObject.getString(CATEGORY_NAME));
                    category.setImage(CategoryObject.getString(CATEGORY_IMAGE));
                    category.setTypeId(CategoryObject.getString(CATEGORY_TYPE_ID));
                    category.setEnabled(CategoryObject.getString(CATEGORY_ENABLED).equals("1"));
                    category.setCreated(CategoryObject.getString(CATEGORY_CREATED));
                    category.setUpdated(CategoryObject.getString(CATEGORY_UPDATED));

                    JSONArray SubCategoryArray = CategoryObject.getJSONArray(CATEGORY_SUBCATEGORY);
                    List<SubCategory> SubCategoryList = new ArrayList<>();

                    for (int j = 0; j < SubCategoryArray.length(); j++) {
                        JSONObject subCategoryObject = SubCategoryArray.getJSONObject(j);
                        SubCategory subCategory = new SubCategory();
                        subCategory.setId(subCategoryObject.getString(SUBCATEGORY_ID));
                        subCategory.setName(subCategoryObject.getString(SUBCATEGORY_NAME));
                        subCategory.setCategoryId(subCategoryObject.getString(SUBCATEGORY_CATEGORY_ID));
                        subCategory.setEnabled(subCategoryObject.getString(SUBCATEGORY_ENABLED).equals("1"));
                        subCategory.setCreated(subCategoryObject.getString(SUBCATEGORY_CREATED_AT));
                        subCategory.setUpdated(subCategoryObject.getString(SUBCATEGORY_UPDATED_AT));
                        SubCategoryList.add(subCategory);
                    }
                    category.setSubCategories(SubCategoryList);
                    CategoryList.add(category);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private URL ConstructURLQuery() throws MalformedURLException {
        final String BASE_URL ="www.getpostman.com";
        final String COLLECTION_PATH ="collections";
        final String ENDPOINT_ID = "2a9f34a396ef5cd2822a";

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https").authority(BASE_URL).
                appendPath(COLLECTION_PATH).
                appendPath(ENDPOINT_ID);
        Uri uri = builder.build();

        return new URL(uri.toString());
    }
}
