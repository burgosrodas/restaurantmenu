package david.burgos.menu.POJO;

import java.util.Date;
import java.util.List;

/**
 * Created by David on 10/07/2015 for project Menu
 */


public class Category {


    private String mId;
    private String mName;
    private String mImage;
    private String mTypeId;
    private Boolean mEnabled;
    private Date mCreated;
    private Date mUpdated;
    private List<SubCategory> mSubCategories;

    public String getTypeId() {
        return mTypeId;
    }

    public void setTypeId(String typeId) {
        mTypeId = typeId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public Boolean getEnabled() {
        return mEnabled;
    }

    public void setEnabled(Boolean enabled) {
        mEnabled = enabled;
    }

    public Date getCreated() {
        return mCreated;
    }

    public void setCreated(String created) {
//        mCreated = new Date(created);
    }

    public Date getUpdated() {
        return mUpdated;
    }

    public void setUpdated(String updated) {
//        mUpdated = new Date(updated);
    }

    public List<SubCategory> getSubCategories() {
        return mSubCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        mSubCategories = subCategories;
    }
}
