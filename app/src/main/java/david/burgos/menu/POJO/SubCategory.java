package david.burgos.menu.POJO;

import java.util.Date;

/**
 * Created by David on 10/07/2015 for project Menu
 */
public class SubCategory {

    private String mId;
    private String mName;
    private String mCategoryId;
    private Boolean mEnabled;
    private Date mCreated;
    private Date mUpdated;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(String categoryId) {
        mCategoryId = categoryId;
    }

    public Boolean getEnabled() {
        return mEnabled;
    }

    public void setEnabled(Boolean enabled) {
        mEnabled = enabled;
    }

    public Date getCreated() {
        return mCreated;
    }

    public void setCreated(String created) {
      //  mCreated = new Date(created);
    }

    public Date getUpdated() {
        return mUpdated;
    }

    public void setUpdated(String updated) {
     //   mUpdated = new Date(updated);
    }
}
